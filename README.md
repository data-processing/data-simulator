# Data Simulator

Data Simulator: Simulates user clicks with timestamp per each screen. As well retrieves statistics data for the user.

For the further information please refer to the platform [official documentation](https://gitlab.com/data-processing/data-processing-meta/-/blob/master/README.md)  
 
