package com.mycompany.dp.simulator.genarator;

import com.mycompany.dp.simulator.config.properties.SimulatorProperties;
import com.mycompany.dp.simulator.config.properties.SimulatorProperties.ScreenProps;
import com.mycompany.dp.simulator.config.properties.SimulatorProperties.UserProps;
import com.mycompany.dp.simulator.pojo.SimulatorEventPojo;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Generates clicks and sends to Data Processor.
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class ClickGenerator {

  private final SimulatorProperties simulatorProperties;

  /**
   * Starts user click requests generation based on application.yml config.
   */
  public void generateClicks() {
    log.info("Simulator generates clicks for config {}", simulatorProperties);
    try {
      startGeneration();
    } catch (Exception ex) {
      log.error("Error during generation", ex);
    }
  }

  private void startGeneration() {
    final List<SimulatorEventPojo> duplicateEvents = new ArrayList<>(
        simulatorProperties.getDuplicateRequestsCount());
    LocalDateTime timeToClick = simulatorProperties.getGenerateForDate().atStartOfDay();
    for (ScreenProps screen : simulatorProperties.getScreens()) {
      for (UserProps user : simulatorProperties.getUsers()) {
        for (int i = 0; i < user.getClickCountPerScreen(); i++) {
          timeToClick = timeToClick.plusSeconds(simulatorProperties.getNextClickInSeconds());
          final SimulatorEventPojo event = generateClick(user.getEmail(),
              user.getName(), screen.getName(), timeToClick);
          if (duplicateEvents.size() < simulatorProperties.getDuplicateRequestsCount()) {
            duplicateEvents.add(event);
          }
        }
      }
    }

    for (SimulatorEventPojo event : duplicateEvents) {
      generateClick(event.getEmail(), event.getName(), event.getScreen(), event.getClickTime());
    }

  }

  private SimulatorEventPojo generateClick(String email, String name,
      String screen, LocalDateTime clickTime) {

    final HttpEntity<SimulatorEventPojo> request = getRequest(email, name, screen,
        clickTime);

    log.info("Generates event for user {}, screen {}, time {}",
        email, screen, clickTime);

    new RestTemplate().postForObject(simulatorProperties.getReceiverUrl(), request, Void.class);

    return request.getBody();
  }

  private HttpEntity<SimulatorEventPojo> getRequest(String email, String name,
      String screen, LocalDateTime clickTime) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("requestId", UUID.randomUUID().toString());

    return new HttpEntity<>(SimulatorEventPojo.builder()
        .email(email)
        .name(name)
        .screen(screen)
        .clickTime(clickTime)
        .build(), headers);
  }
}
