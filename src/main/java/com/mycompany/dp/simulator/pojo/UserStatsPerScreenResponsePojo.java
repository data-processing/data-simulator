package com.mycompany.dp.simulator.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserStatsPerScreenResponsePojo {

  private String email;

  private String screen;

  private Double avgClickTimeInSeconds;

}
