package com.mycompany.dp.simulator.pojo;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SimulatorEventPojo {

  private String email;

  private String name;

  private String screen;

  private LocalDateTime clickTime;

}
