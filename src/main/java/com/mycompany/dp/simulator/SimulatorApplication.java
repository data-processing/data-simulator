package com.mycompany.dp.simulator;

import com.mycompany.dp.simulator.genarator.ClickGenerator;
import com.mycompany.dp.simulator.statistics.StatisticsCalculator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SimulatorApplication implements CommandLineRunner {

  @Autowired
  private ClickGenerator generator;

  @Autowired
  private StatisticsCalculator calculator;

  public static void main(String[] args) {
    SpringApplication.run(SimulatorApplication.class, args);
  }

  @Override
  public void run(String... args) throws InterruptedException {
    generator.generateClicks();
    calculator.calculateStatistics();
  }
}