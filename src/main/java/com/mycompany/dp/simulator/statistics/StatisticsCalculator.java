package com.mycompany.dp.simulator.statistics;

import com.mycompany.dp.simulator.config.properties.SimulatorProperties;
import com.mycompany.dp.simulator.config.properties.SimulatorProperties.ScreenProps;
import com.mycompany.dp.simulator.config.properties.SimulatorProperties.UserProps;
import com.mycompany.dp.simulator.pojo.UserStatsPerScreenResponsePojo;
import com.mycompany.dp.simulator.pojo.UserStatsResponsePojo;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Slf4j
@Component
public class StatisticsCalculator {

  private final SimulatorProperties simulatorProperties;

  public void calculateStatistics() throws InterruptedException {
    // Wait for certain seconds in order give time for the system finalize generation of statistics
    TimeUnit.SECONDS.sleep(simulatorProperties.getWaitForGenerationInSeconds());
    log.info("Statistics retrieving for config {}", simulatorProperties);

    for (UserProps user : simulatorProperties.getUsers()) {
      assertUserStatistic(user);
      for (ScreenProps screen : simulatorProperties.getScreens()) {
        assertUserScreenStatistic(user, screen);
      }
    }
  }

  private void assertUserStatistic(UserProps user) {
    try {
      log.info("Get user stats for {}", user.getEmail());
      final UserStatsResponsePojo userStats = new RestTemplate().exchange(
          simulatorProperties.getAnalyzerUserStatsUrl(),
          HttpMethod.GET, getHeaders(), UserStatsResponsePojo.class, user.getEmail(),
          simulatorProperties.getGenerateForDate().format(DateTimeFormatter.ISO_DATE)).getBody();
      if (userStats == null) {
        return;
      }
      if (user.getClickCountPerScreen() == userStats.getAvgClickCount().intValue()) {
        log.info("Expected average click count for user {} succeeded", user.getName());
      } else {
        log.error("Expected average click count for user {} failed, expected {} actual {}",
            user.getName(), user.getClickCountPerScreen(), userStats.getAvgClickCount());
      }
    } catch (Exception ex) {
      log.error("Error during user statistics {}", ex.getMessage());
    }

  }

  private void assertUserScreenStatistic(UserProps user, ScreenProps screen) {
    try {
      log.info("Get user stats for {} and screen {}", user.getEmail(), screen.getName());
      final UserStatsPerScreenResponsePojo userStats = new RestTemplate().exchange(
          simulatorProperties.getAnalyzerUserScreenStatsUrl(),
          HttpMethod.GET, getHeaders(), UserStatsPerScreenResponsePojo.class, user.getEmail(), screen.getName(),
          simulatorProperties.getGenerateForDate().format(DateTimeFormatter.ISO_DATE)).getBody();
      if (userStats == null) {
        return;
      }
      if (simulatorProperties.getNextClickInSeconds() == userStats.getAvgClickTimeInSeconds().intValue()) {
        log.info("Expected average screen time for user {} and screen {} succeeded", user.getName(), screen.getName());
      } else {
        log.error("Expected average screen time for user {} and screen {} failed, expected {} actual {}",
            user.getName(), screen.getName(), simulatorProperties.getNextClickInSeconds(),
            userStats.getAvgClickTimeInSeconds().intValue());
      }
    } catch (Exception ex) {
      log.error("Error during user/screen statistics", ex);
    }
  }

  private HttpEntity<?> getHeaders() {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("requestId", UUID.randomUUID().toString());

    return new HttpEntity<>(headers);
  }
}
