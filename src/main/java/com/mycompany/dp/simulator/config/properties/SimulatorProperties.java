package com.mycompany.dp.simulator.config.properties;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "simulator")
public class SimulatorProperties {

  @NotEmpty
  private List<@NotNull UserProps> users;

  @NotEmpty
  private List<@NotNull ScreenProps> screens;

  @Min(1)
  @NotNull
  private Integer duplicateRequestsCount;

  @URL
  @NotBlank
  private String receiverUrl;

  @URL
  @NotBlank
  private String analyzerUserStatsUrl;

  @URL
  @NotBlank
  private String analyzerUserScreenStatsUrl;

  @NotNull
  private String generateForDateAsString;

  @NotNull
  private Integer nextClickInSeconds;

  @NotNull
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private Integer waitForGenerationInSeconds;

  public LocalDate getGenerateForDate() {
    return LocalDate.parse(generateForDateAsString, DateTimeFormatter.ISO_DATE);
  }

  @Data
  public static class UserProps {

    @Email
    @Size(max = 100)
    @NotBlank
    private String email;

    @Size(max = 255)
    @NotBlank
    private String name;

    @Min(1)
    @NotNull
    private Integer clickCountPerScreen;
  }

  @Data
  public static class ScreenProps {

    @Size(max = 100)
    @NotBlank
    private String name;
  }

}
